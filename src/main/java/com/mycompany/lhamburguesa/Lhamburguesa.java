package com.mycompany.lhamburguesa;

/**
 *
 * @author Gemma Ordax
 */
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Lhamburguesa {

    public static void main(String[] args) {
        generarPaginaInicial();
    }

    public static void generarPaginaInicial() {
        String nomFitxer = "indexlhamburguesa.html";

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(nomFitxer))) {
            bw.write("<!doctype html>\n");
            bw.write("<html lang=\"en\">\n");
            bw.write("<head>\n");
            bw.write("    <meta charset=\"utf-8\">\n");
            bw.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
            bw.write("    <title>L'hamburguesa</title>\n");
            bw.write("    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65\" crossorigin=\"anonymous\">\n");
            bw.write("</head>\n");
            bw.write("<body>\n");
            bw.write("    <h2>Gemma Ordax Torramilas</h2>\n");
            bw.write("    <h1>La meva hamburguesa</h1>\n");
            bw.write("    <h4>He après molt fent el mòdul 8 de DAW encara que hagi estat dur!!!</h4>\n");
            bw.write("    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4\" crossorigin=\"anonymous\"></script>\n");
            bw.write("</body>\n");
            bw.write("</html>\n");
            System.out.println("La pàgina s'ha generat correctament al fitxer " + nomFitxer);
        } catch (IOException e) {
            System.err.println("Error al generar la pàgina HTML: " + e.getMessage());
        }
    }
}

